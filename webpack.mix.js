const mix = require('laravel-mix');
const CopyWebpackPlugin = require('copy-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .webpackConfig({
        plugins: [
            new CopyWebpackPlugin([
                {
                    from: './node_modules/@dvsl/zoomcharts/lib/assets',
                    to: 'assets'
                },
                {
                    from: './node_modules/bootstrap/dist',
                    to: 'bootstrap'
                },
                {
                    from: './node_modules/font-awesome/css',
                    to: 'font-awesome/css'
                },
                {
                    from: './resources/img',
                    to: 'img'
                },
                {
                    from: './node_modules/jquery/dist',
                    to: 'jquery'
                }
            ]),
        ]
    })
    .js('resources/js/app.js', 'public/bundle.js')
