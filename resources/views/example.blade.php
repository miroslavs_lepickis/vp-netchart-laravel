<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VP Zoomcharts NetChart Example</title>

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
    <style>
        .DVSL-bar-bottom a.DVSL-bar-btn {
            padding: 0 6px;
        }

        .DVSL-bar-bottom a.DVSL-bar-btn p {
            padding-left: 25px;
        }

        #popup-search {
            position: absolute;
            right: 10px;
            top: 10px;
            padding: 15px;
            background: #d0d0d0;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div id="app">
    <div id="chart-container">
        <div id="chart"></div>
        <div id="popup-editor" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="popup-editor"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit node</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="node-editor">
                                <div class="form-group">
                                    <label for="name">Node name</label>
                                    <input type="text" class="form-control" id="name" placeholder="node.data.name">
                                </div>
                                <div class="form-group">
                                    <label for="subtitle">Node subtitle</label>
                                    <input type="text" class="form-control" id="subtitle"
                                           placeholder="node.data.subtitle">
                                </div>
                                <div class="form-group">
                                    <label for="auras">Auras (separated by a space)</label>
                                    <input type="text" class="form-control" id="auras" placeholder="node.data.auras">
                                </div>
                            </div>
                            <div class="link-editor">
                                <div class="form-group">
                                    <label for="label">Link label</label>
                                    <input type="text" class="form-control" id="label" placeholder="link.label">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="saveNodeOrLink()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="popup-search">
            <form>
                <div class="form-group">
                    <label for="search">Search node</label>
                    <input type="text" class="form-control" id="search" placeholder="node.data.name"
                           onkeyup="searchNode()">
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('bundle.js') }}"></script>
<script src="{{ asset('jquery/jquery.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
</body>
</html>
