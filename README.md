#### VP Zoomcharts

##### Installation

- Database
```
//.env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

- npm
```
npm install
npm run dev
```

- config
```
// config/net-chart-config.php

return [
    'netChartRules' => [
        [
            'cyber_cube_identity' => 'name',
            'cyber_cube_location' => 'location',
            'upload_data' => 'upload_record',
        ],
        [
            'cyber_cube_identity' => 'birth_date',
            'upload_data' => 'upload_record',
        ],
        [
            'cyber_cube_identity' => 'phone_number',
            'upload_data' => 'upload_record',
        ],
    ],
    'netChartEntitySubtitles' => [
        'cyber_cube_identity' => [
            'name' => 'Name',
            'email_address' => 'Email',
            'register_ID' => 'Registration number',
            'web_page' => 'Web page',
            'birth_date' => 'Birth date',
        ],
        'cyber_cube_location' => [
            'location' => 'Location',
        ],
    ]
];
```

##### Api

```
GET /api/chart/data
```
