<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeIdentity
 *
 * @ORM\Table(name="cyber_cube_identity", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeIdentity extends Model
{
    public const TABLE_NAME = 'cyber_cube_identity';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_identity';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_identity';

    public function getIdIdentity(): ?int
    {
        return $this->ID_identity;
    }

    public function getName(): string
    {
        return $this->name ?? 'UNKNOWN';
    }

    public function getBirthDate(): ?string
    {
        return $this->birth_date;
    }

    public function getRegisterId(): ?string
    {
        return $this->register_ID;
    }

    public function getVictim(): ?string
    {
        return $this->victim;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function getWebPage(): ?string
    {
        return $this->web_page;
    }

    public function getWalletId(): ?string
    {
        return $this->wallet_id;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
