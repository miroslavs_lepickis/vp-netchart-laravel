<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeWeb
 *
 * @ORM\Table(name="cyber_cube_web", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeWeb extends Model
{
    public const TABLE_NAME = 'cyber_cube_web';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_web';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_web';

    public function getIdWeb(): ?int
    {
        return $this->ID_web;
    }

    public function hasWebAddress(): bool
    {
        return (bool)$this->webAddress;
    }

    public function getWebAddress(): ?string
    {
        return $this->web_address;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
