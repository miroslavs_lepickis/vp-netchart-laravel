<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * UploadData
 *
 * @ORM\Table(name="upload_data", indexes={@ORM\Index(name="cyber_cube_ID", columns={"cyber_cube_ID"})})
 * @ORM\Entity
 */
class UploadData extends Model
{
    public const TABLE_NAME = 'upload_data';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'upload_ID';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'upload_data';

    public function getUploadId(): ?int
    {
        return $this->upload_ID;
    }

    public function getUploadRecord(): ?string
    {
        return $this->upload_record;
    }

    public function getFileName(): ?string
    {
        return $this->file_name;
    }

    public function getCyberCube(): ?int
    {
        return $this->cyber_cube_ID;
    }
}
