<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubePhone
 *
 * @ORM\Table(name="cyber_cube_phone", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubePhone extends Model
{
    public const TABLE_NAME = 'cyber_cube_phone';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_phone';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_phone';

    public function getIdPhone(): ?int
    {
        return $this->ID_phone;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
