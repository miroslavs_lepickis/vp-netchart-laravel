<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeEmail
 *
 * @ORM\Table(name="cyber_cube_email", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeEmail extends Model
{
    public const TABLE_NAME = 'cyber_cube_email';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_email';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_email';

    public function getIdEmail(): ?int
    {
        return $this->ID_email;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
