<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeWallet
 *
 * @ORM\Table(name="cyber_cube_wallet", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeWallet extends Model
{
    public const TABLE_NAME = 'cyber_cube_wallet';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_wallet';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_wallet';

    public function getIdWallet(): ?int
    {
        return $this->ID_wallet;
    }

    public function getPaymentId(): ?string
    {
        return $this->payment_ID;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
