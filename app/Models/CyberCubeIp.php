<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeIp
 *
 * @ORM\Table(name="cyber_cube_ip", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeIp extends Model
{
    public const TABLE_NAME = 'cyber_cube_ip';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_ip';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_ip';

    public function getIdIp(): ?int
    {
        return $this->ID_ip;
    }

    public function getIpAddress(): ?string
    {
        return $this->IP_address;
    }

    public function getIpDate(): ?string
    {
        return $this->IP_date;
    }

    public function getIpTime(): ?string
    {
        return $this->IP_time;
    }

    public function getIdNrMain(): ?int
    {
        return $this->ID_nr_main;
    }
}
