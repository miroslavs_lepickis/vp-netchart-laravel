<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeLocation
 *
 * @ORM\Table(name="cyber_cube_location", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeLocation extends Model
{
    public const TABLE_NAME = 'cyber_cube_location';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_location';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_location';

    public function getIdLocation(): ?int
    {
        return $this->ID_location;
    }

    public function hasLocation(): bool
    {
        return (bool) $this->location;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getIdNrMain()
    {
        return $this->ID_nr_main;
    }
}
