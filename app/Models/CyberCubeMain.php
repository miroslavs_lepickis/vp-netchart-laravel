<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CyberCubeMain
 *
 * @ORM\Table(name="cyber_cube_main")
 * @ORM\Entity
 */
class CyberCubeMain extends Model
{
    public const TABLE_NAME = 'cyber_cube_main';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID_nr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cyber_cube_main';

    public function getIdNr(): ?int
    {
        return $this->ID_nr;
    }

    public function getRecievingType(): ?string
    {
        return $this->recieving_type;
    }

    public function getRecievingCountry(): ?string
    {
        return $this->recieving_country;
    }

    public function getRecievingDate(): ?string
    {
        return $this->recieving_date;
    }

    public function getRecievingNr(): ?string
    {
        return $this->recieving_nr;
    }

    public function getCrimeType(): ?string
    {
        return $this->crime_type;
    }

    public function getDamage(): ?string
    {
        return $this->damage;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function getWriterId(): ?string
    {
        return $this->writer_ID;
    }

    public function getWriterDateTime(): ?string
    {
        return $this->writer_date_time;
    }

    public function identities()
    {
        return $this->hasMany(CyberCubeIdentity::class, 'ID_nr_main', 'ID_nr');
    }

    public function locations()
    {
        return $this->hasMany(CyberCubeLocation::class, 'ID_nr_main', 'ID_nr');
    }

    public function phones()
    {
        return $this->hasMany(CyberCubePhone::class, 'ID_nr_main', 'ID_nr');
    }

    public function emails()
    {
        return $this->hasMany(CyberCubeEmail::class, 'ID_nr_main', 'ID_nr');
    }

    public function webAddresses()
    {
        return $this->hasMany(CyberCubeWeb::class, 'ID_nr_main', 'ID_nr');
    }

    public function wallets()
    {
        return $this->hasMany(CyberCubeWallet::class, 'ID_nr_main', 'ID_nr');
    }

    public function ipAddresses()
    {
        return $this->hasMany(CyberCubeIp::class, 'ID_nr_main', 'ID_nr');
    }

    public function uploads()
    {
        return $this->hasMany(UploadData::class, 'cyber_cube_ID', 'ID_nr');
    }
}
