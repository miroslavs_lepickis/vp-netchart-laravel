<?php

namespace App\Services;

use App\Models\CyberCubeEmail;
use App\Models\CyberCubeIdentity;
use App\Models\CyberCubeIp;
use App\Models\CyberCubeLocation;
use App\Models\CyberCubeMain;
use App\Models\CyberCubePhone;
use App\Models\CyberCubeWallet;
use App\Models\CyberCubeWeb;
use App\Models\UploadData;

class ChartService
{
    private array $rules;
    private array $subtitles;

    public function __construct()
    {
        $this->rules = config('net-chart-config.netChartRules', []);
        $this->subtitles = config('net-chart-config.netChartEntitySubtitles', []);
    }

    public function applyNetChartDataSubtitles(array $data): array
    {
        foreach ($this->subtitles as $tableName => $columns) {
            foreach ($columns as $columnName => $subtitle) {
                foreach ($data['nodes'] as $i => $nodeItem) {
                    if (array_key_exists('extra', $nodeItem)) {
                        if ($nodeItem['extra']['table_name'] === $tableName && $nodeItem['extra']['column_name'] === $columnName) {
                            $nodeItem['extra']['subtitle'] = $subtitle;

                            $data['nodes'][$i] = $nodeItem;
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function applyNetChartDataRules(array $data): array
    {
        foreach ($this->rules as $tables) {
            $links = [];
            foreach ($tables as $tableName => $columnName) {
                foreach ($data['nodes'] as $i => $nodeItem) {
                    if (
                        array_key_exists('extra', $nodeItem)
                        && $nodeItem['extra']['table_name'] === $tableName
                        && $nodeItem['extra']['column_name'] === $columnName
                    ) {
                        $value = $nodeItem['name'];
                        if (array_key_exists($value, $links)) {
                            $item = $links[$value];
                            $linkKey = array_search(
                                $nodeItem['id'] . '-' . $tableName,
                                array_column($data['links'], 'id')
                            );
                            if ($linkKey) {
                                continue;
                            }

                            $data['links'][] = [
                                'id' => $nodeItem['id'] . '-' . $tableName,
                                'from' => $nodeItem['id'],
                                'to' => $item['extra']['chain'],
                            ];

                            $nodeKey = array_search($item['id'], array_column($data['nodes'], 'id'));
                            $data['nodes'][$nodeKey]['className'] = 'hidden';

                            $nodeItem['auras'] = $nodeItem['auras'] . ' ' . $item['auras'];
                            $data['nodes'][] = $nodeItem;
                        }

                        if ([] === $links || !array_key_exists($value, $links)) {
                            $links[$value] = $nodeItem;
                        }
                    }
                }
            }
        }

        return $data;
    }

    private function getNetChartDataStructure(): array
    {
        return [
            'nodes' => [],
            'links' => [],
        ];
    }

    /**
     * Scary method
     * Should be refactored
     */
    public function getNetChartData(): array
    {
        $data = $this->getNetChartDataStructure();
        $collection = CyberCubeMain::with([
            'emails',
            'identities',
            'ipAddresses',
            'locations',
            'phones',
            'wallets',
            'webAddresses',
            'uploads'
        ])->get();

        $case = $aura = 'c';
        $link = 'l';

        foreach ($collection as $cyberCubeMain) {
            $data['nodes'][] = [
                'id' => $case . '-' . $cyberCubeMain->getIdNr(),
                'loaded' => true,
                'name' => $cyberCubeMain->getRecievingNr() . ' ' . $cyberCubeMain->getRecievingType(),
                'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                'className' => 'classCase'
            ];

            $identities = $cyberCubeMain->identities;
            if (!$identities->isEmpty()) {
                $tableName = CyberCubeIdentity::TABLE_NAME;

                $hasIdentities = false;

                /** @var CyberCubeIdentity $item */
                foreach ($identities as $item) {
                    $chain = $tableName . '-' . $item->getIdIdentity() . '-name';

                    if ((bool)$item->getBirthDate()) {
                        $columnName = 'birth_date';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getBirthDate(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];

                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getRegisterId()) {
                        $columnName = 'register_ID';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getRegisterId(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getAddress()) {
                        $columnName = 'address';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getAddress(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getPhoneNumber()) {
                        $columnName = 'phone_number';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getPhoneNumber(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getEmailAddress()) {
                        $columnName = 'email_address';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getEmailAddress(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getWebPage()) {
                        $columnName = 'web_page';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getWebPage(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ((bool)$item->getWalletId()) {
                        $columnName = 'wallet_id';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getWalletId(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $chain
                        ];
                        $hasIdentities = true;
                    }

                    if ($hasIdentities) {
                        $data['nodes'][] = [
                            'id' => $case . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName,
                            'loaded' => true,
                            'name' => 'Identity',
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'className' => 'classIdentities'
                        ];

                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName,
                            'from' => $case . '-' . $cyberCubeMain->getIdNr(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName,
                        ];

                        $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName;
                        $columnName = 'name';
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'loaded' => true,
                            'name' => $item->getName(),
                            'className' => (bool)$item->getName() ? 'unknown' : '',
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => $columnName,
                                'chain' => $chain,
                            ]
                        ];

                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'from' => $tableName . '-' . $item->getIdIdentity() . '-' . $columnName,
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName
                        ];
                    }
                }
            }

            $hasInfo = false;

            $locations = $cyberCubeMain->locations;
            if (!$locations->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-info';
                $tableName = CyberCubeLocation::TABLE_NAME;

                /** @var CyberCubeLocation $item */
                foreach ($locations as $item) {
                    if ((bool)$item->getLocation()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdLocation(),
                            'loaded' => true,
                            'name' => $item->getLocation(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'location',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdLocation(),
                            'from' => $tableName . '-' . $item->getIdLocation(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                        ];
                        $hasInfo = true;
                    }
                }
            }

            $phones = $cyberCubeMain->phones;
            if (!$phones->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-info';
                $tableName = CyberCubePhone::TABLE_NAME;

                /** @var CyberCubePhone $item */
                foreach ($phones as $item) {
                    if ((bool)$item->getPhoneNumber()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdPhone(),
                            'loaded' => true,
                            'name' => $item->getPhoneNumber(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'phone_number',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdPhone(),
                            'from' => $tableName . '-' . $item->getIdPhone(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                        ];
                        $hasInfo = true;
                    }
                }
            }

            $emails = $cyberCubeMain->emails;
            if (!$emails->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-info';
                $tableName = CyberCubeEmail::TABLE_NAME;

                /** @var CyberCubeEmail $item */
                foreach ($emails as $item) {
                    if ((bool)$item->getEmailAddress()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdEmail(),
                            'loaded' => true,
                            'name' => $item->getEmailAddress(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'email_address',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdEmail(),
                            'from' => $tableName . '-' . $item->getIdEmail(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                        ];
                        $hasInfo = true;
                    }
                }
            }

            $webAddresses = $cyberCubeMain->webAddresses;
            if (!$webAddresses->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-info';
                $tableName = CyberCubeWeb::TABLE_NAME;

                /** @var CyberCubeWeb $item */
                foreach ($webAddresses as $item) {
                    if ((bool)$item->getWebAddress()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdWeb(),
                            'loaded' => true,
                            'name' => $item->getWebAddress(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'web_address',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdWeb(),
                            'from' => $tableName . '-' . $item->getIdWeb(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                        ];
                        $hasInfo = true;
                    }
                }
            }

            $wallets = $cyberCubeMain->wallets;
            if (!$wallets->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-info';
                $tableName = CyberCubeWallet::TABLE_NAME;

                /** @var CyberCubeWallet $item */
                foreach ($wallets as $item) {
                    if ((bool)$item->getPaymentId()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdWallet(),
                            'loaded' => true,
                            'name' => $item->getPaymentId(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'payment_ID',
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdWallet(),
                            'from' => $tableName . '-' . $item->getIdWallet(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                        ];
                        $hasInfo = true;
                    }
                }
            }

            if ($hasInfo) {
                $data['nodes'][] = [
                    'id' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                    'loaded' => true,
                    'name' => 'Info',
                    'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                    'className' => 'classOtherInformation'
                ];

                $data['links'][] = [
                    'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-info',
                    'from' => $case . '-' . $cyberCubeMain->getIdNr(),
                    'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-info',
                ];
            }

            $ipAddresses = $cyberCubeMain->ipAddresses;
            if (!$ipAddresses->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-ipAddress';
                $tableName = CyberCubeIp::TABLE_NAME;

                $hasIpAddress = false;

                /** @var CyberCubeIp $item */
                foreach ($ipAddresses as $item) {
                    if ((bool)$item->getIpAddress()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $item->getIdIp(),
                            'loaded' => true,
                            'name' => $item->getIpAddress() . ' ' . $item->getIpDate(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'ip_address',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $tableName . '-' . $item->getIdIp(),
                            'from' => $tableName . '-' . $item->getIdIp(),
                            'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-ipAddress',
                        ];
                        $hasIpAddress = true;
                    }
                }

                if ($hasIpAddress) {
                    $data['nodes'][] = [
                        'id' => $case . '-' . $cyberCubeMain->getIdNr() . '-ipAddress',
                        'loaded' => true,
                        'name' => 'IpAddress',
                        'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                        'className' => 'classIpAddresses'
                    ];

                    $data['links'][] = [
                        'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-ipAddress',
                        'from' => $case . '-' . $cyberCubeMain->getIdNr(),
                        'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-ipAddress',
                    ];
                }
            }

            $uploads = $cyberCubeMain->uploads;

            if (!$uploads->isEmpty()) {
                $chain = $case . '-' . $cyberCubeMain->getIdNr() . '-uploadData';
                $tableName = UploadData::TABLE_NAME;

                $data['nodes'][] = [
                    'id' => $case . '-' . $cyberCubeMain->getIdNr() . '-uploadData',
                    'loaded' => true,
                    'name' => 'UploadData',
                    'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                    'className' => 'classUploads'
                ];

                $data['links'][] = [
                    'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-uploadData',
                    'from' => $case . '-' . $cyberCubeMain->getIdNr(),
                    'to' => $case . '-' . $cyberCubeMain->getIdNr() . '-uploadData',
                ];

                /** @var UploadData $item */
                foreach ($uploads as $item) {
                    if ((bool)$item->getUploadRecord()) {
                        $data['nodes'][] = [
                            'id' => $tableName . '-' . $cyberCubeMain->getIdNr() . '-' . $item->getUploadId(),
                            'loaded' => true,
                            'name' => $item->getUploadRecord(),
                            'auras' => $aura . '-' . $cyberCubeMain->getIdNr(),
                            'extra' => [
                                'table_name' => $tableName,
                                'column_name' => 'upload_record',
                                'chain' => $chain,
                            ],
                        ];
                        $data['links'][] = [
                            'id' => $link . '-' . $cyberCubeMain->getIdNr() . '-' . $item->getUploadId() . '-ipAddress',
                            'from' => $tableName . '-' . $cyberCubeMain->getIdNr() . '-' . $item->getUploadId(),
                            'to' => $chain,
                        ];
                    }
                }
            }
        }

        return $data;
    }
}
