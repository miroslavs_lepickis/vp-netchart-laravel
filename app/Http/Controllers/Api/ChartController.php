<?php

namespace App\Http\Controllers\Api;

use App\Services\ChartService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ChartController extends BaseController
{
    private ChartService $chartService;

    public function __construct(ChartService $chartService)
    {
        $this->chartService = $chartService;
    }

    /**
     * @Route("/api/chart/data", name="api.chart.data")
     */
    public function data(): JsonResponse
    {
        // Structured data
        $data = $this->chartService->getNetChartData();

        // Structured data with rules applied
        $dataWithRulesApplied = $this->chartService->applyNetChartDataRules($data);

        // Structured data with rules and subtitles applied
        $dataWithSubtitleApplied = $this->chartService->applyNetChartDataSubtitles($dataWithRulesApplied);

        return new JsonResponse($dataWithSubtitleApplied);
    }
}
