<?php

namespace App\Http\Controllers;

use App\Models\CyberCubeEmail;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Show zoomcharts netchart example page.
     *
     * @return View
     */
    public function example(): View
    {
        return view('example');
    }
}
