<?php

return [
    'netChartRules' => [
        [
            'cyber_cube_identity' => 'name',
            'cyber_cube_location' => 'location',
            'upload_data' => 'upload_record',
        ],
        [
            'cyber_cube_identity' => 'birth_date',
            'upload_data' => 'upload_record',
        ],
        [
            'cyber_cube_identity' => 'phone_number',
            'upload_data' => 'upload_record',
        ],
    ],
    'netChartEntitySubtitles' => [
        'cyber_cube_identity' => [
            'name' => 'Name',
            'email_address' => 'Email',
            'register_ID' => 'Registration number',
            'web_page' => 'Web page',
            'birth_date' => 'Birth date',
        ],
        'cyber_cube_location' => [
            'location' => 'Location',
        ],
    ]
];
